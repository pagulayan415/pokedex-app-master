import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Homepage from "./Pages/Homepage";
import "./App.css";
import { PokemonProvider } from "./PokemonContext";
import Pokemon from "./Pages/Pokemon";

const App = () => {
  return (
    <PokemonProvider>
      <Router>
        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="/status/:name/:id" element={<Pokemon />} />
        </Routes>
      </Router>
    </PokemonProvider>
  );
};

export default App;
