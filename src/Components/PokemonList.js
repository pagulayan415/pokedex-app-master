import { useContext } from "react";
import PokemonThumbnail from "./PokemonThumbnail";
import { Button } from "react-bootstrap";
import { PokemonContext } from "../PokemonContext";
import { Link } from "react-router-dom";

const PokemonList = () => {
  const [allPokemons, getAllPokemons] = useContext(PokemonContext);
  // console.log(allPokemons);

  return (
    <div className="pokemon-container">
      <div>
        {allPokemons.map((pokemon, index) => (
          <Link to={`/status/${pokemon.name}/${index}`} key={pokemon.id}>
            <Button name={index}>
              <PokemonThumbnail
                name={pokemon.name}
                image={pokemon.sprites.other.dream_world.front_default}
                index={index}
              />
            </Button>
          </Link>
        ))}
      </div>
      <button
        className="load-more"
        onClick={() => {
          getAllPokemons();
        }}
      >
        Load More
      </button>
    </div>
  );
};

export default PokemonList;
