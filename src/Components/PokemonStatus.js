import React from "react";
import { Container, Row, Col} from "react-bootstrap";
import { Link } from "react-router-dom";

const PokemonStatus = ({ stats }) => {
  return (
    <Container>
      <Row>
        <Col><img src={stats.sprites.other.dream_world.front_default} alt={stats.name} className="pokemonStatusImage"></img></Col>
        <Col>
          <h1> NAME: {stats.name}</h1>
          <h2> Type: {stats.types.map((poke, i) =>{
            return(
              <React.Fragment key={i}>
                "{poke.type.name}"
              </React.Fragment>
            );
          })}</h2>
          <h2> Ablities: {stats.abilities.map((poke, i) =>{
              return(
                <React.Fragment key={i}>
                  "{poke.ability.name}"
                </React.Fragment>
              )
          })}</h2>
        </Col>
      </Row>
      <Row>
        <Col><h1>Stats</h1> {stats.stats.map((poke, i) =>{
          return(
            <React.Fragment key={i}>
              <h4>{poke.stat.name} : {poke.base_stat}</h4>
            </React.Fragment>
          );
        })}</Col>
      </Row>
      <Link to={"../"}>
         <button className="goBack">Go Back</button>
       </Link>
</Container>
    // <>
    // <row xs={12} className="mt-4">
    //   <div className="pImage">
    //     <img
    //       src={stats.sprites.other.dream_world.front_default}
    //       alt={stats.name}
    //       className="pokemonStatusImage"
    //     ></img>
    //   </div>
    //   <div>
    //         <h1> NAME: {stats.name}</h1>
    //   </div>
    // </row>
    //   <div className="ablities">
    //     <h2>
    //       Status:
    //     </h2>
    //     <h3>Example:</h3>
    //     <h4>
    //       <span style={{ color: "red" }}>Weight:</span> {stats.weight}
    //     </h4>
    //     <h4>
    //       <span style={{ color: "red" }}>Height:</span> {stats.height}
    //     </h4>
    //   </div>
    //   
    // </>
  );
};

export default PokemonStatus;
