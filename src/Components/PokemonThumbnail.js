import React from "react";
import { Col, Card } from "react-bootstrap";

const PokemonThumbnail = (props) => {
  const { id, name, image } = props;
  return (
    <Col xs={12} className="mt-4 pokemonList" id={name}>
      <Card className="card1">
        {id}
        <img src={image} alt={name} className="pokemonImage"></img>
        {name}
      </Card>
    </Col>
  );
};

export default PokemonThumbnail;
